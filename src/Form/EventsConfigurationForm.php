<?php

namespace Drupal\kafka_event_publisher\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\kafka_event_publisher\Utils;

// @todo form validation
// @todo broker url testing before saving it.

class EventsConfigurationForm extends ConfigFormBase {
  public const CONFIG_ID = 'kafka_event_publisher.settings';

  public function getFormId() {
    return 'kafka_event_publisher_events_config_settings';
  }

  public function getEditableConfigNames() {
    return [
      EventsConfigurationForm::CONFIG_ID,
    ];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(EventsConfigurationForm::CONFIG_ID);

    $form['kafka'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Configure Kafka Event Publisher settings'),
      '#collapsible' => false,
      '#collapsed' => false,
      '#prefix' => $this->t('Apache Kafka is an open-source distributed event streaming platform and one of well known use cases for
        Kafka is a message broker which are used for variety of reasons, e.g. decoupling processing from data producers. Kafka Event Publisher
        module leverages this use case of Kafka and helps to decouple the processing of Drupal/Symfony events to Kafka.'),
    );

    $form['kafka']['kafka_enable'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Kafka Event Publishing'),
      '#description' => $this->t('Once enabled the mapped Drupal/Symfony events start publishing to Kafka topics.'),
      '#default_value' => $config->get('kafka_enable'),
    );

    $form['kafka']['kafka_broker_list'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Kafka brokers'),
      '#attributes' => array('placeholder' => $this->t('10.0.0.1:9092, 10.0.0.2:9092')),
      '#description' => $this->t('Enter Kafka brokers by comma(,) separated list (e.g.: 10.0.0.1:9092, 10.0.0.2:9092). A broker
        is a Kafka server and Kafka cluster typically consists of multiple brokers to maintain load balance.'),
      '#default_value' => $config->get('kafka_broker_list'),
      '#required' => true,
    );

    $form['kafka']['kafka_topic_event_list'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Kafka topic & Drupal events mapping'),
      '#description' => $this->t("Map Kafka topics to Drupal Events. Supported format kafka_topic|entity:event i.e. article_notify|article:create. Each
        mapping must be separted by a newline. Currently supported Drupal Events are entity:insert, entity:update, entity:delete, entity:view."),
      '#placeholder' => $this->t('topic_name|entity:event'),
      '#default_value' => Utils::decodeEvents($config->get('kafka_topic_event_list')),
      '#rows' => 10,
      '#required' => true,
    );

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(EventsConfigurationForm::CONFIG_ID)
      ->set('kafka_enable', $form_state->getValue('kafka_enable'))
      ->set('kafka_topic_event_list', Utils::encodeEvents($form_state->getValue('kafka_topic_event_list')))
      ->set('kafka_broker_list', $form_state->getValue('kafka_broker_list'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
