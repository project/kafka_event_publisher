<?php

namespace Drupal\kafka_event_publisher;

use Drupal\Core\Entity\EntityInterface;
use Drupal\kafka_event_publisher\Form\EventsConfigurationForm;

class Utils {
  public static function decodeEvents($events) {
    if (!$events) {
      return;
    }
    $list = unserialize($events);
    $list_str = [];
    foreach ($list as $key => $value) {
      $list_str[] = implode('|', $value);
    }

    return implode(PHP_EOL, $list_str);
  }

  public static function encodeEvents($events) {
    $list = explode(PHP_EOL, $events);
    $key_value = [];
    foreach ($list as $key => $value) {
      $list_element = explode('|', $value);
      $key_value[$key] = ['topic' => trim($list_element[0]), 'event' => trim($list_element[1])];
    }

    return serialize($key_value);
  }

  /**
   * Get event type of entity base on events
   *
   * @param string $event
   * @param EntityInterface $entity
   *
   * @return string
   */
  public static function getEventType($event, EntityInterface $entity) {
    $entity_type = $entity->bundle();

    switch ($event) {
      case 'insert':
        return $entity_type . ':create';
      default:
        return $entity_type . ':' . $event;

    }
  }

  public static function isKafkaEnable() {
    $kafka_enable = \Drupal::config(EventsConfigurationForm::CONFIG_ID)->get('kafka_enable');

    return $kafka_enable || false;
  }

  public static function logger() {
    return \Drupal::logger('kafka_event_publisher');
  }
}
