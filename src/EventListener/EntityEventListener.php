<?php

namespace Drupal\kafka_event_publisher\EventListener;

use Symfony\Component\EventDispatcher\Event;

class EntityEventListener {
  protected $kafkaTopic;

  public function __construct($kafkaTopic) {
    $this->kafkaTopic = $kafkaTopic;
  }

  public function onEntityEvent(Event $event) {
    $payload = [
      'entity' => (array) $event->getEntity(),
      'event_type' => $event->getEventType(),
    ];
    \Drupal::service('kafka_event_publisher.kafkaproducer')->produce($this->kafkaTopic, $payload);
  }
}
