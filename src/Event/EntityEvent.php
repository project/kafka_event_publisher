<?php

namespace Drupal\kafka_event_publisher\Event;

use Drupal\Core\Entity\EntityInterface;
use Drupal\event_dispatchers\EntityEventDispatcherType;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class to contain an entity event.
 */
class EntityEvent extends Event {

  /**
   * The Entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  private $_entity;

  /**
   * The event type.
   *
   * @var \Drupal\event_dispatcher\EntityEventDispatcherType
   */
  private $_eventType;

  /**
   * Construct a new entity event.
   *
   * @param string                              $event_type
   *   The event type.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity which caused the event.
   */
  public function __construct($event_type, EntityInterface $entity) {
    $this->_entity = $entity;
    $this->_eventType = $event_type;
  }

  /**
   * Method to get the entity from the event.
   */
  public function getEntity() {
    return $this->_entity;
  }

  /**
   * Method to get the event type.
   *
   * @return mixed
   */
  public function getEventType() {
    return $this->_eventType;
  }
}
