<?php

namespace Drupal\kafka_event_publisher\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\kafka_event_publisher\EventListener\EntityEventListener;
use Drupal\kafka_event_publisher\Form\EventsConfigurationForm;
use Drupal\kafka_event_publisher\Utils;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class KafkaEventSubscriber implements EventSubscriberInterface {
  protected $dispatcher;
  protected $config_factory;

  public function __construct(ConfigFactoryInterface $config_factory, EventDispatcherInterface $dispatcher) {
    $this->dispatcher = $dispatcher;
    $this->config_factory = $config_factory;
  }

  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['registerEntityEventListeners'];

    return $events;
  }

  public function registerEntityEventListeners() {
    
    if (!Utils::isKafkaEnable()) {
      return;
    }
    $events = $this->config_factory->get(EventsConfigurationForm::CONFIG_ID)->get('kafka_topic_event_list');
    $events = unserialize($events);

    foreach ($events as $event) {
      $this->addKafkaEventListener($event['topic'], $event['event']);
    }
  }

  public function addKafkaEventListener($topic, $event) {
    $listener = new EntityEventListener($topic);
    $this->dispatcher->addListener($event, [$listener, 'onEntityEvent']);
  }

}
