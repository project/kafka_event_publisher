<?php

namespace Drupal\kafka_event_publisher\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\kafka_event_publisher\Form\EventsConfigurationForm;
use Drupal\kafka_event_publisher\Utils;
use RdKafka\Producer;

class KafkaProducer {
  private $_config_factory;
  private $_producer;

  /**
   * Kafka constructer
   *
   * @param ConfigFactoryInterface $config_factory config fectory
   */
  public function __construct(ConfigFactoryInterface $config_factory, Producer $producer) {
    $this->_config_factory = $config_factory->get(EventsConfigurationForm::CONFIG_ID)->getRawData();
    $this->_producer = $producer;
    $this->_producer->addBrokers($this->_config_factory['kafka_broker_list']);
  }

  /**
   * Produce to kafka topic
   *dasd
   * @param [string] $topic kafka topic
   * @param [object] $data  data to send to kfaka
   *
   * @return void
   */
  public function produce($topic, $data) {
    try {
      $kafka_topic = $this->_producer->newTopic($topic);
      $kafka_topic->produce(RD_KAFKA_PARTITION_UA, 0, json_encode(['data' => $data]), 0);
    } catch (\Exception $e) {
      Utils::logger($e->getMessage());
    }
  }
}
