<?php
namespace Drupal\Tests\kafka_event_publisher\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\kafka_event_publisher\EventListener\EntityEventListener;
use Drupal\kafka_event_publisher\Event\EntityEvent;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\kafka_event_publisher\Service\KafkaProducer;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

class EntityEventListenerTest extends UnitTestCase {
  use ProphecyTrait;

  public function testShouldListenerProduceDataToKafkaTopic() {
    $topic = 'article_notify';
    $entityEvent = $this->prophesize(EntityEvent::class);
    $kafkaProducer = $this->prophesize(KafkaProducer::class);
    $kafkaProducer->produce(Argument::type('string'), [])->willReturn(true);
    $container = new ContainerBuilder();
    $container->set('kafka_event_publisher.kafkaproducer', $kafkaProducer->reveal());
    \Drupal::setContainer($container);


    $listener = new EntityEventListener($topic);
    $listener->onEntityEvent($entityEvent->reveal());

    $entityEvent->getEntity()->shouldBeCalled();
    $entityEvent->getEventType()->shouldBeCalled();
    $kafkaProducer->produce($topic, ["entity" => [], "event_type" => null])->shouldBeCalled();
 }
}
