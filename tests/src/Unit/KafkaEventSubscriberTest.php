<?php

namespace Drupal\Tests\kafka_event_publisher\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\kafka_event_publisher\EventSubscriber\KafkaEventSubscriber;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\kafka_event_publisher\Form\EventsConfigurationForm;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Argument;
use Drupal\kafka_event_publisher\EventListener\EntityEventListener;

class KafkaEventSubscriberTest extends UnitTestCase {
  use ProphecyTrait;

  protected $config_factory_stub;
  protected $kafkaEventSubscriber;
  protected $dispatcherStub;
  protected $configFactory;
  protected $container;

  public function setUp():void {
    $config = [];
    $config[EventsConfigurationForm::CONFIG_ID] = [
      'kafka_topic_event_list' => KafkaEventSubscriberTest::getEncodedEvents(),
      'kafka_enable' => true,
    ];

    $this->configFactory = $this->getConfigFactoryStub($config);
    $this->container = new ContainerBuilder();
    $this->container->set('config.factory', $this->configFactory);
    \Drupal::setContainer($this->container);
    $this->dispatcherStub = $this->prophesize(EventDispatcherInterface::class);

    $this->kafkaEventSubscriber = new KafkaEventSubscriber($this->configFactory, $this->dispatcherStub->reveal());
  }

  public function testGetSubscribedEventsSubscribeRegisterEntityEventListenersOnKernelRequest(){
    $subcriptions = KafkaEventSubscriber::getSubscribedEvents();
    $this->assertArrayHasKey(KernelEvents::REQUEST, $subcriptions);
    $this->assertStringContainsString('registerEntityEventListeners', json_encode($subcriptions[KernelEvents::REQUEST]));
  }

  public function testregisterEntityEventListeners() {
    $this->kafkaEventSubscriber->registerEntityEventListeners();
    $this->dispatcherStub->addListener('article:create', [
      new EntityEventListener('article_notify'),
      'onEntityEvent',
    ])->shouldBeCalled();
    $this->dispatcherStub->addListener('article:update', [
      new EntityEventListener('article_notify'),
      'onEntityEvent',
    ])->shouldBeCalled();
  }

  public function testShouldNotAddListenerIfDisableFromConfig() {
    $config = [];
    $config[EventsConfigurationForm::CONFIG_ID] = [
      'kafka_topic_event_list' => KafkaEventSubscriberTest::getEncodedEvents(),
      'kafka_enable' => false,
    ];

    $configFactory = $this->getConfigFactoryStub($config);

    $this->container->set('config.factory', $configFactory);
    $kafkaEventSubscriber = new KafkaEventSubscriber($configFactory, $this->dispatcherStub->reveal());

    $this->kafkaEventSubscriber->registerEntityEventListeners();
    $this->dispatcherStub->addListener('article:create', [
      new EntityEventListener('article_notify'),
      'onEntityEvent',
    ])->shouldNotBeCalled();
    $this->dispatcherStub->addListener('article:update', [
      new EntityEventListener('article_notify'),
      'onEntityEvent',
    ])->shouldNotBeCalled();
  }

  static public function getEncodedEvents() {
    return serialize(
      [
        [
          "topic" => 'article_notify',
          "event" => 'article:create'
        ],
        [
          'topic' => 'article_notify',
          'event' => 'article:update'
        ]
        ]
    );
  }
}