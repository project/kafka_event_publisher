<?php
namespace Drupal\Tests\kafka_event_publisher\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\Core\Entity\EntityInterface;
use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\kafka_event_publisher\Utils;
use Drupal\kafka_event_publisher\Form\EventsConfigurationForm;

class UtilsTest extends UnitTestCase {
  use ProphecyTrait;

  public function testencodeEvents() {
    $events = Utils::encodeEvents($this->getDecodedEvents());
    $this->assertEquals($events, $this->getEncodedEvents());
  }

  public function testdecodeEvents() {
    $events = Utils::decodeEvents($this->getEncodedEvents());
    $this->assertEquals($events, $this->getDecodedEvents());
  }

  public function testShouldgetEventTypeReturnCreateEventForInsert() {
    $stubEntity = $this->prophesize(EntityInterface::class);
    $stubEntity->bundle()->willReturn('article');
    $event = Utils::getEventType('insert', $stubEntity->reveal());
    $this->assertEquals($event, 'article:create');
  }

  public function testShouldgetEventTypeReturnHookNameAsEvent() {
    $stubEntity = $this->prophesize(EntityInterface::class);
    $stubEntity->bundle()->willReturn('article');
    $event = Utils::getEventType('update', $stubEntity->reveal());
    $this->assertEquals($event, 'article:update');
  }

  public function getDecodedEvents() {
    return implode(PHP_EOL, [
      'article_notify|article:create',
      'article_notify|article:update',
    ]);
  }

  public function getEncodedEvents() {
    return serialize(
      [
        [
          "topic" => 'article_notify',
          "event" => 'article:create'
        ],
        [
          'topic' => 'article_notify',
          'event' => 'article:update'
        ]
        ]
    );
  }

}