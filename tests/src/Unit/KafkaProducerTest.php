<?php 
namespace Drupal\Tests\kafka_event_publisher\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\kafka_event_publisher\Service\KafkaProducer;
use RdKafka\Producer;
use RdKafka\ProducerTopic;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Argument;


class KafkaProducerTest extends UnitTestCase{
  use ProphecyTrait;

  protected $kafkaProducer;
  protected $config_factory_stub;
  protected $producerspy;
  protected $producer;
  protected $topicInstanceStub;

  public function setUp():void {
    $stubConfig = new Class{
      public function getRawData() {
        return ['kafka_broker_list' => 'broker:9092'];
      }
    };

    $this->topicInstanceStub = $this->prophesize(ProducerTopic::class);
    $this->topicInstanceStub->produce();

    $this->producerspy = $this->prophesize(Producer::class);
    $this->producerspy->addBrokers(Argument::type('string'))->willReturn(true);
    $this->producerspy->newTopic(Argument::type('string'))->willReturn($this->topicInstanceStub->reveal());

    $this->config_factory_stub = $this->createStub(ConfigFactoryInterface::class);
    $this->config_factory_stub->method('get')
                        ->willReturn($stubConfig);
    $this->kafkaProducer = new KafkaProducer($this->config_factory_stub, $this->producerspy->reveal());
  }

    public function testConstructerShouldAddBrokers()
    {
      $this->producerspy->addBrokers('broker:9092')->shouldHaveBeenCalledTimes(1);
    }

    public function testProducerShouleProduceDataToGivenTopic()
    {
      $this->kafkaProducer->produce('testtopic', 'hello');
      $this->producerspy->newTopic(Argument::exact('testtopic'))->shouldHaveBeenCalledTimes(1);
      $this->topicInstanceStub->produce(RD_KAFKA_PARTITION_UA, 0, json_encode(['data' => 'hello']), 0)->shouldHaveBeenCalledTimes(1);
    }
}
